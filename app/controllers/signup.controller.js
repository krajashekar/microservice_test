import {users} from '../models';
import nodemailer from 'nodemailer';
async function signup(req,res){
	
	const {name,email,password,gender,mobile}= req.body;

	if (!name || !email || !password || !gender || !mobile) {
	    res.status(400).send({ status:0, message: "Name, Email, Password, Gender, Mobile No can not be empty!" })
	}
	const stepStatus = 0
	const activeStatus = false
	const user = new users ({
	    name,
	    email,
	    password,
	    gender,
	    mobile,
	    stepStatus,
	    activeStatus
	});
	users.find({ email})
    .then(data => {
    	if(data.length == 0) {
			user
		    .save(user)
		    .then(async(data) => {
		    	const link = `http://localhost:8080/auth/user/${data.id}`
		    	await verifyEmail(email,name,link)
		      	res.send({ status:1,data});
		    })
		    .catch(err => {
		      res.status(500).send({
		        message:
		          err.message || "Some error occurred while creating the Tutorial."
		      });
		    });
		} else {
	   		 res.status(400).send({ status:0, message: "Email ID Already Exist" });
		}
	})
}
async function verifyEmail(email,name,link){
	return new Promise(async function (resolve,reject) {
		let mailcontent = `Hi ${name}, <br/> <br/>
		you have successfully registered.  <br/>
		To verify your email please <a href="${link}">click here</a>.  <br/>
		Thank you.`	
		let testAccount = await nodemailer.createTestAccount();

	  // create reusable transporter object using the default SMTP transport
	  let transporter = nodemailer.createTransport({
		host: 'smtp.gmail.com',
		port: 587,
		secure: false, // true for 465, false for other ports
		auth: {
		  user: 'noreplyreceiptscan@gmail.com', // generated ethereal user
		  pass: 'kolkdpavblcezlun' // generated ethereal password
		},   
	  });

	  // send mail with defined transport object
	  let info = await transporter.sendMail({
		from: '"Welcome Email from Micro" <pbjchandrika@gmail.com>', // sender address
		to: email, // list of receivers
		subject: `Email Verification`, // Subject line
		text: "Email Verification", // plain text body
		html: mailcontent // html body
	  });
		
	  console.log("Message sent: %s", info.messageId);
		  if(info.messageId) {
			 resolve(info.messageId)
		  } else {
			reject('error')
		  }

	  // Preview only available when sending through an Ethereal account
	  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
	});
}
module.exports={
	signup: signup
}