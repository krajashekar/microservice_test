import {users} from '../models';
async function login(req,res){
	const email = req.body.email
	const password = req.body.password	
	if (!email || !password) {
	    res.status(400).send({ status:0, message: "Email or Password can not be empty!" })
	}	
	users.find({ email,password })
    .then(data => {
      res.send({ status:1,data});
    }).catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
}
module.exports={
	login: login
}